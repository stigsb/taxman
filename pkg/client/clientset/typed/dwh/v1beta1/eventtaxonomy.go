// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Code generated by client-gen. DO NOT EDIT.

package v1beta1

import (
	"time"

	v1beta1 "gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/apis/dwh/v1beta1"
	scheme "gitlab.com/zedge-oss/zeppa/taxman/pkg/client/clientset/scheme"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
)

// EventTaxonomiesGetter has a method to return a EventTaxonomyInterface.
// A group's client should implement this interface.
type EventTaxonomiesGetter interface {
	EventTaxonomies(namespace string) EventTaxonomyInterface
}

// EventTaxonomyInterface has methods to work with EventTaxonomy resources.
type EventTaxonomyInterface interface {
	Create(*v1beta1.EventTaxonomy) (*v1beta1.EventTaxonomy, error)
	Update(*v1beta1.EventTaxonomy) (*v1beta1.EventTaxonomy, error)
	UpdateStatus(*v1beta1.EventTaxonomy) (*v1beta1.EventTaxonomy, error)
	Delete(name string, options *v1.DeleteOptions) error
	DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error
	Get(name string, options v1.GetOptions) (*v1beta1.EventTaxonomy, error)
	List(opts v1.ListOptions) (*v1beta1.EventTaxonomyList, error)
	Watch(opts v1.ListOptions) (watch.Interface, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1beta1.EventTaxonomy, err error)
	EventTaxonomyExpansion
}

// eventTaxonomies implements EventTaxonomyInterface
type eventTaxonomies struct {
	client rest.Interface
	ns     string
}

// newEventTaxonomies returns a EventTaxonomies
func newEventTaxonomies(c *DwhV1beta1Client, namespace string) *eventTaxonomies {
	return &eventTaxonomies{
		client: c.RESTClient(),
		ns:     namespace,
	}
}

// Get takes name of the eventTaxonomy, and returns the corresponding eventTaxonomy object, and an error if there is any.
func (c *eventTaxonomies) Get(name string, options v1.GetOptions) (result *v1beta1.EventTaxonomy, err error) {
	result = &v1beta1.EventTaxonomy{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("eventtaxonomies").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of EventTaxonomies that match those selectors.
func (c *eventTaxonomies) List(opts v1.ListOptions) (result *v1beta1.EventTaxonomyList, err error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	result = &v1beta1.EventTaxonomyList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("eventtaxonomies").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Do().
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested eventTaxonomies.
func (c *eventTaxonomies) Watch(opts v1.ListOptions) (watch.Interface, error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	opts.Watch = true
	return c.client.Get().
		Namespace(c.ns).
		Resource("eventtaxonomies").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Watch()
}

// Create takes the representation of a eventTaxonomy and creates it.  Returns the server's representation of the eventTaxonomy, and an error, if there is any.
func (c *eventTaxonomies) Create(eventTaxonomy *v1beta1.EventTaxonomy) (result *v1beta1.EventTaxonomy, err error) {
	result = &v1beta1.EventTaxonomy{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("eventtaxonomies").
		Body(eventTaxonomy).
		Do().
		Into(result)
	return
}

// Update takes the representation of a eventTaxonomy and updates it. Returns the server's representation of the eventTaxonomy, and an error, if there is any.
func (c *eventTaxonomies) Update(eventTaxonomy *v1beta1.EventTaxonomy) (result *v1beta1.EventTaxonomy, err error) {
	result = &v1beta1.EventTaxonomy{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("eventtaxonomies").
		Name(eventTaxonomy.Name).
		Body(eventTaxonomy).
		Do().
		Into(result)
	return
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().

func (c *eventTaxonomies) UpdateStatus(eventTaxonomy *v1beta1.EventTaxonomy) (result *v1beta1.EventTaxonomy, err error) {
	result = &v1beta1.EventTaxonomy{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("eventtaxonomies").
		Name(eventTaxonomy.Name).
		SubResource("status").
		Body(eventTaxonomy).
		Do().
		Into(result)
	return
}

// Delete takes name of the eventTaxonomy and deletes it. Returns an error if one occurs.
func (c *eventTaxonomies) Delete(name string, options *v1.DeleteOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("eventtaxonomies").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *eventTaxonomies) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	var timeout time.Duration
	if listOptions.TimeoutSeconds != nil {
		timeout = time.Duration(*listOptions.TimeoutSeconds) * time.Second
	}
	return c.client.Delete().
		Namespace(c.ns).
		Resource("eventtaxonomies").
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Timeout(timeout).
		Body(options).
		Do().
		Error()
}

// Patch applies the patch and returns the patched eventTaxonomy.
func (c *eventTaxonomies) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1beta1.EventTaxonomy, err error) {
	result = &v1beta1.EventTaxonomy{}
	err = c.client.Patch(pt).
		Namespace(c.ns).
		Resource("eventtaxonomies").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}
