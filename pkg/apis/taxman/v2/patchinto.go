// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package v2

import (
	"time"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
)

// The "PatchInto" methods implement the patching part of the "get-patch-put"
// pattern used by taxman.
//
// The logic implemented here does not do any change validation, it only implements
// an adapter between the (simpler) API taxonomy and the full model taxonomy,
// making changes to the model taxonomy based on the request body, without concerning
// itself about whether the changes are allowed.
//
// The actual validation is handled by model.TaxonomyUpdate.
//
func (e Enum) PatchInto(dstEnum *model.EventEnum, twinEnum *model.EventEnum) {
	if e.SameAs(dstEnum) {
		return
	}
	dstEnum.Name = e.Name
	for _, field := range e.Fields {
		dstField := dstEnum.GetField(field.Name)
		if dstField == nil {
			// ADD field
			var value int
			if field.Value == nil {
				value = getNextEnumFieldValue(dstEnum, twinEnum)
			} else {
				value = *field.Value
			}
			dstField = &model.EventEnumField{
				Name:         field.Name,
				Value:        value,
				Lifecycle:    model.PreProductionLifecycle,
				LastModified: v1.Time{Time: time.Now()},
			}
			dstEnum.Fields = append(dstEnum.Fields, dstField)
		} else {
			// MODIFY field
			field.PatchInto(dstField)
		}
	}
	for _, dstField := range dstEnum.Fields {
		if _, found := e.fieldNameIndex[dstField.Name]; !found {
			// DELETE field
			dstField.EndLife()
		}
	}

	dstEnum.BuildIndex()
}

func (f EnumField) PatchInto(dstField *model.EventEnumField) {
	if f.SameAs(dstField) {
		return
	}
	dstField.Name = f.Name
	dstField.LastModified = v1.Time{Time: time.Now()}
	dstField.Revive()
	if f.Value != nil {
		dstField.Value = *f.Value
	}
}

func (e Enum) SameAs(enum *model.EventEnum) bool {
	if e.Name != enum.Name || len(e.Fields) != len(enum.Fields) {
		return false
	}
	for i, f := range e.Fields {
		if !f.SameAs(enum.Fields[i]) {
			return false
		}
		if !enum.Fields[i].Lifecycle.IsAlive() {
			return false
		}
	}
	return true
}

func (f EnumField) SameAs(field *model.EventEnumField) bool {
	if f.Name != field.Name {
		return false
	}
	// Treat an api EnumField without a value as the same as a model EnumField
	if f.Value != nil && *f.Value != field.Value {
		return false
	}
	if !field.Lifecycle.IsAlive() {
		return false
	}
	return true
}

func (p Property) PatchInto(dstProp *model.EventProperty) {
	if p.SameAs(dstProp) && dstProp.Lifecycle.IsAlive() {
		return
	}
	dstProp.Name = p.Name
	dstProp.Scope = *p.Scope
	dstProp.Type = p.Type
	dstProp.LastModified = v1.Time{Time: time.Now()}
	dstProp.Revive()
}

func (p Property) SameAs(prop *model.EventProperty) bool {
	return p.Name == prop.Name && p.Type == prop.Type && *p.Scope == prop.Scope
}

func (t Taxonomy) PatchInto(dst *model.EventTaxonomy) {
	t.BuildIndex()
	dst.BuildIndex()
	dst.AppID = t.AppID
	for _, prop := range t.Properties {
		dstProp := dst.GetProperty(prop.Name)
		if dstProp == nil {
			// ADD property
			scope := model.EventScope
			if prop.Scope != nil {
				scope = *prop.Scope
			}
			dstProp = &model.EventProperty{
				Name:         prop.Name,
				Scope:        scope,
				Lifecycle:    model.PreProductionLifecycle,
				LastModified: v1.Time{Time: time.Now()},
				Type:         prop.Type,
			}
			dst.Properties = append(dst.Properties, dstProp)
		} else {
			// MODIFY property
			prop.PatchInto(dstProp)
		}
	}
	for _, dstProp := range dst.Properties {
		if _, found := t.propertyMap[dstProp.Name]; !found {
			// DELETE property
			// Envelope and InternalScope can have been added server side and only lives in the model.
			// If we shall allow deletion here, we need to namespace the Internal and Envelope variant to contain a server side variant.
			if !dstProp.Scope.IsSacred() {
				dstProp.EndLife()
			}
		}
	}
	for _, enum := range t.Enums {
		dstEnum := dst.GetEnum(enum.Name)
		if dstEnum == nil {
			// ADD enum
			dstFields := make([]*model.EventEnumField, len(enum.Fields))
			for i, field := range enum.Fields {
				dstFields[i] = &model.EventEnumField{
					Name:         field.Name,
					Value:        i + 1,
					Lifecycle:    model.PreProductionLifecycle,
					LastModified: v1.Time{Time: time.Now()},
				}
			}
			dstEnum = &model.EventEnum{
				Name:   enum.Name,
				Fields: dstFields,
			}
			dst.Enums = append(dst.Enums, dstEnum)
		} else {
			// MODIFY enum
			var twinEnum *model.EventEnum
			if t.twin != nil {
				t.twin.BuildIndex()
				twinEnum = t.twin.GetEnum(enum.Name)
			}
			enum.PatchInto(dstEnum, twinEnum)
		}
	}
	enumsToDelete := make([]string, 0)
	for _, dstEnum := range dst.Enums {
		if _, found := t.enumMap[dstEnum.Name]; !found {
			// DELETE enum
			enumsToDelete = append(enumsToDelete, dstEnum.Name)
		}
	}
	for _, enumToDelete := range enumsToDelete {
		dst.RemoveEnum(enumToDelete)
	}
	dst.BuildIndex()
}

func getNextEnumFieldValue(enums ...*model.EventEnum) int {
	next := 1
start:
	for _, e := range enums {
		if e != nil {
			for _, f := range e.Fields {
				if f.Value == next {
					next++
					goto start
				}
			}
		}
	}
	return next
}
