// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package v2

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
	dwhv1beta1clientset "gitlab.com/zedge-oss/zeppa/taxman/pkg/client/clientset/typed/dwh/v1beta1"
)

type Handlers struct {
	Namespace string
	Client    dwhv1beta1clientset.EventTaxonomyInterface
	Log       *zap.SugaredLogger
}

func (h *Handlers) ListUserPropertiesHandler(w http.ResponseWriter, r *http.Request) {
	userProperties, err := getUserPropertiesForAllTaxonomies(h.Client)
	if err != nil {
		writeErrorResponse(http.StatusInternalServerError, w, err)
	}

	writeJSONResponse(http.StatusOK, w, &userProperties)
}

func (h *Handlers) UpdateTaxonomyHandler(w http.ResponseWriter, r *http.Request) {
	kubeTaxonomy, err, statusCode := getKubeTaxonomy(h.Client, appidFromReq(r))
	if err != nil {
		writeErrorResponse(statusCode, w, err)
		return
	}
	modelTaxonomy := &kubeTaxonomy.Spec.Taxonomy
	modelTaxonomy.BuildIndex()
	var twinModelTaxonomy *model.EventTaxonomy
	if kubeTaxonomy.Spec.TwinTaxonomy != "" {
		twinCr, err := h.Client.Get(kubeTaxonomy.Spec.TwinTaxonomy, metav1.GetOptions{})
		if err != nil {
			code := http.StatusInternalServerError
			if errors.IsNotFound(err) {
				code = http.StatusNotFound
			}
			writeErrorResponse(code, w, err)
			return
		}
		twinModelTaxonomy = &twinCr.Spec.Taxonomy
		twinModelTaxonomy.BuildIndex()
	}
	var apiTaxonomy Taxonomy
	if err = json.NewDecoder(r.Body).Decode(&apiTaxonomy); err != nil {
		writeErrorResponse(http.StatusBadRequest, w, err)
	}
	apiTaxonomy.twin = twinModelTaxonomy
	apiTaxonomy.ApplyDefaults()
	apiTaxonomy.BuildIndex()
	newModelTaxonomy := modelTaxonomy.DeepCopy()
	apiTaxonomy.PatchInto(newModelTaxonomy)
	changes := model.ComputeChanges(modelTaxonomy, newModelTaxonomy)
	resp := NewPublishTaxonomyResponse()
	resp.AddIssues(changes.Validate()...)
	if twinModelTaxonomy != nil {
		resp.AddIssues(newModelTaxonomy.ValidateTwin(*twinModelTaxonomy)...)
	}
	if len(resp.Issues) > 0 {
		writeJSONResponse(http.StatusBadRequest, w, resp)
		return
	}

	if r.URL.Query().Get("dry-run") == "true" {
		writeJSONResponse(http.StatusOK, w, msg("successfully updated (dry run)"))
		return
	}
	newModelTaxonomy.DeepCopyInto(&kubeTaxonomy.Spec.Taxonomy)
	_, err = h.Client.Update(kubeTaxonomy)
	if err != nil {
		h.Log.Error("Error while updating EventTaxonomy", "name", kubeTaxonomy.Name, "Namespace", kubeTaxonomy.Namespace, "error", err.Error())
		writeErrorResponse(http.StatusInternalServerError, w, err)
		return
	}
	writeJSONResponse(http.StatusOK, w, msg("successfully updated"))
}

func (h *Handlers) GetTaxonomyHandler(w http.ResponseWriter, r *http.Request) {
	taxonomy := Taxonomy{}
	kubeTaxonomy, err, statusCode := getKubeTaxonomy(h.Client, appidFromReq(r))
	if err != nil {
		writeErrorResponse(statusCode, w, err)
		return
	}
	taxonomy.CopyFromModel(&kubeTaxonomy.Spec.Taxonomy)
	taxonomy.ApplyDefaults()
	writeJSONResponse(http.StatusOK, w, &taxonomy)
}

func (h *Handlers) Register(r *mux.Router) {
	r.HandleFunc("/v2/taxonomy/{appid}", h.GetTaxonomyHandler).Methods("GET")
	r.HandleFunc("/v2/taxonomy/{appid}", h.UpdateTaxonomyHandler).Methods("PUT")
	r.HandleFunc("/v2/user-properties", h.ListUserPropertiesHandler).Methods("GET")
}
