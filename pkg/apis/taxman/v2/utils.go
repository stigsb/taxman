// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package v2

import (
	"encoding/json"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
	"net/http"

	"github.com/gorilla/mux"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/apis/dwh/v1beta1"
	dwhv1beta1clientset "gitlab.com/zedge-oss/zeppa/taxman/pkg/client/clientset/typed/dwh/v1beta1"
)

func appidFromReq(r *http.Request) string {
	v := mux.Vars(r)
	return v["appid"]
}

func writeJSONResponse(s int, w http.ResponseWriter, body interface{}) {
	w.WriteHeader(s)
	w.Header().Set("Content-type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.SetEscapeHTML(false)
	_ = encoder.Encode(body)
}
func msg(m string) msgResponse {
	return msgResponse{m}
}

type msgResponse struct {
	Message string `json:"msg"`
}

func writeErrorResponse(s int, w http.ResponseWriter, err error) {
	writeJSONResponse(s, w, msg(err.Error()))
}

func getKubeTaxonomy(client dwhv1beta1clientset.EventTaxonomyInterface, appID string) (*v1beta1.EventTaxonomy, error, int) {
	taxonomy, err := client.Get(appID, metav1.GetOptions{})
	if err != nil {
		httpStatusCode := http.StatusInternalServerError
		if errors.IsNotFound(err) {
			httpStatusCode = http.StatusNotFound
		}
		return nil, err, httpStatusCode
	}
	return taxonomy, nil, 0
}

func getUserProperties(properties []*model.EventProperty) []Property {
	result := make([]Property, 0)
	for _, property := range properties {
		if property.Scope != model.UserScope && property.Scope != model.InternalScope {
			continue
		}

		result = append(result, Property{Name: property.Name, Type: property.Type})
	}
	return result
}

func getUserPropertiesForAllTaxonomies(client dwhv1beta1clientset.EventTaxonomyInterface) (ListUserPropertiesForTaxonomiesResponse, error) {
	taxonomies, err := client.List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	response := make(ListUserPropertiesForTaxonomiesResponse)
	for _, taxonomy := range taxonomies.Items {
		userProperties := getUserProperties(taxonomy.Spec.Taxonomy.Properties)
		if len(userProperties) > 0 {
			response[AppID(taxonomy.Spec.Taxonomy.AppID)] = userProperties
		}
	}

	return response, nil
}
