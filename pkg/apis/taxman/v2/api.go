// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package v2

import (
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
)

type Taxonomy struct {
	AppID       string     `json:"appid"`
	Enums       []Enum     `json:"enums,omitempty"`
	Properties  []Property `json:"properties"`
	enumMap     map[string]*Enum
	propertyMap map[string]*Property
	twin        *model.EventTaxonomy
}

type Enum struct {
	Name           string      `json:"name"`
	Fields         []EnumField `json:"fields"`
	fieldNameIndex map[string]*EnumField
}

type EnumField struct {
	Name  string `json:"name"`
	Value *int   `json:"value"`
}

type Property struct {
	Name  string               `json:"name"`
	Type  string               `json:"type"`
	Scope *model.PropertyScope `json:"scope,omitempty"`
}

type PublishTaxonomyRequest struct {
	Taxonomy `json:",inline"`
}

type PublishTaxonomyResponse struct {
	Issues []string `json:"issues"`
}

type AppID string
type ListUserPropertiesForTaxonomiesResponse map[AppID][]Property

type ValidateTaxonomyRequest struct {
	Taxonomy `json:",inline"`
}

type ValidateTaxonomyResponse struct {
	Issues []string `json:"issues"`
}

func (t *Taxonomy) BuildIndex() {
	t.enumMap = make(map[string]*Enum)
	t.propertyMap = make(map[string]*Property)
	for i := range t.Properties {
		t.propertyMap[t.Properties[i].Name] = &t.Properties[i]
	}
	for i := range t.Enums {
		t.enumMap[t.Enums[i].Name] = &t.Enums[i]
		t.Enums[i].BuildIndex()
	}
}

func (t *Taxonomy) ApplyDefaults() {
	var eventScopeString = model.EventScope

	for i, property := range t.Properties {
		if property.Scope == nil {
			t.Properties[i].Scope = &eventScopeString
		}
	}
}

func (e *Enum) BuildIndex() {
	e.fieldNameIndex = make(map[string]*EnumField)
	for i := range e.Fields {
		e.fieldNameIndex[e.Fields[i].Name] = &e.Fields[i]
	}
}

func NewPublishTaxonomyResponse() *PublishTaxonomyResponse {
	return &PublishTaxonomyResponse{Issues: make([]string, 0)}
}

func (r PublishTaxonomyResponse) AddIssues(issues ...error) {
	for _, err := range issues {
		r.Issues = append(r.Issues, err.Error())
	}
}
