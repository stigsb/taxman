// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package taxman

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
	"gitlab.com/zedge-oss/zeppa/taxman/pkg/client/clientset/typed/dwh/v1beta1"
)

type Handlers struct {
	Namespace string
	Client    v1beta1.EventTaxonomyInterface
	Log       *zap.SugaredLogger
}

type putTaxonomyResponse struct {
	Issues  []string              `json:"issues"`
	Changes *model.TaxonomyUpdate `json:"changes"`
}

func (h *Handlers) Register(r *mux.Router) {
	r.HandleFunc("/", h.homeHandler)

	// API v1
	r.HandleFunc("/taxonomy/{appid}", h.getTaxonomyHandler).Methods("GET")
	r.HandleFunc("/taxonomy/{appid}", h.putTaxonomyHandler).Methods("PUT")
	r.HandleFunc("/taxonomy", h.validateTaxonomyHandler).Methods("POST").Queries("validate", "true")

	r.HandleFunc("/topics/{appid}", h.getTopicsHandler).Methods("GET")

	r.Path("/metrics").Handler(promhttp.Handler())
}

func (h *Handlers) homeHandler(w http.ResponseWriter, r *http.Request) {

	writeJSONResponse(http.StatusOK, w, okResponse)
}

// putTaxonomyHandler updates an existing taxonomy after validating that the update
// follows all of the migration rules.
func (h *Handlers) putTaxonomyHandler(w http.ResponseWriter, r *http.Request) {
	appID := appidFromReq(r)
	if appID == "" {
		writeErrorResponse(http.StatusBadRequest, w, fmt.Errorf("could not extract appid"))
		return
	}
	txCr, err := h.Client.Get(appidFromReq(r), metav1.GetOptions{})
	if err != nil {
		code := http.StatusInternalServerError
		if errors.IsNotFound(err) {
			code = http.StatusNotFound
		}
		writeErrorResponse(code, w, err)
		return
	}
	oldTxPtr := &txCr.Spec.Taxonomy
	oldTxPtr.BuildIndex()
	var newTx model.EventTaxonomy
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&newTx); err != nil {
		writeErrorResponse(http.StatusBadRequest, w, err)
		return
	}
	if err = newTx.OK(); err != nil {
		writeErrorResponse(http.StatusBadRequest, w, err)
		return
	}
	newTx.BuildIndex()

	// We dont let new taxonomy via API override blacklist.
	newTx.BlacklistPropertiesFilter = oldTxPtr.BlacklistPropertiesFilterNames()

	changes := model.ComputeChanges(oldTxPtr, &newTx)
	resp := &putTaxonomyResponse{make([]string, 0), changes}
	for _, issue := range changes.Validate() {
		resp.Issues = append(resp.Issues, issue.Error())
	}
	if txCr.Spec.TwinTaxonomy != "" {
		twinCr, err := h.Client.Get(txCr.Spec.TwinTaxonomy, metav1.GetOptions{})
		if err != nil {
			code := http.StatusInternalServerError
			if errors.IsNotFound(err) {
				code = http.StatusNotFound
			}
			writeErrorResponse(code, w, fmt.Errorf("while fetching twin %q: %w", txCr.Spec.TwinTaxonomy, err))
			return
		}
		twinTx := twinCr.Spec.Taxonomy
		twinTx.BuildIndex()
		for _, issue := range newTx.ValidateTwin(twinTx) {
			resp.Issues = append(resp.Issues, issue.Error())
		}
	}
	if len(resp.Issues) > 0 {
		writeJSONResponse(http.StatusBadRequest, w, resp)
		return
	}
	if r.URL.Query().Get("dry-run") != "" {
		return
	}
	newTx.DeepCopyInto(&txCr.Spec.Taxonomy)
	_, err = h.Client.Update(txCr)
	if err != nil {
		h.Log.Error("Error while updating EventTaxonomy", "name", txCr.Name, "Namespace", txCr.Namespace, "error", err.Error())
		writeErrorResponse(http.StatusInternalServerError, w, err)
		return
	}
	writeJSONResponse(http.StatusOK, w, msg("successfully updated"))
}

func (h *Handlers) getTaxonomyHandler(w http.ResponseWriter, r *http.Request) {
	et, err := h.Client.Get(appidFromReq(r), metav1.GetOptions{})
	if err != nil {
		code := http.StatusInternalServerError
		if errors.IsNotFound(err) {
			code = http.StatusNotFound
		}
		writeErrorResponse(code, w, err)
		return
	}
	writeJSONResponse(http.StatusOK, w, &et.Spec.Taxonomy)
}

func (h *Handlers) validateTaxonomyHandler(w http.ResponseWriter, r *http.Request) {
	var new model.EventTaxonomy
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&new); err != nil {
		writeErrorResponse(http.StatusBadRequest, w, err)
		return
	}
	if err := new.OK(); err != nil {
		writeErrorResponse(http.StatusBadRequest, w, err)
		return
	}
	writeJSONResponse(http.StatusOK, w, okResponse)
}

func (h *Handlers) getTopicsHandler(w http.ResponseWriter, r *http.Request) {
	et, err := h.Client.Get(appidFromReq(r), metav1.GetOptions{})
	if err != nil {
		code := http.StatusInternalServerError
		if errors.IsNotFound(err) {
			code = http.StatusNotFound
		}
		writeErrorResponse(code, w, err)
		return
	}
	resp := &TopicsResponse{Topics: make([]string, 0)}
	if et.Spec.Kafka != nil && et.Spec.Kafka.TopicBindings != nil {
		for _, b := range et.Spec.Kafka.TopicBindings {
			resp.Topics = append(resp.Topics, b.Name)
		}
	}
	writeJSONResponse(http.StatusOK, w, resp)
}

var okResponse = msg("OK")

func msg(m string) msgResponse {
	return msgResponse{m}
}

type msgResponse struct {
	Message string `json:"msg"`
}

func writeJSONResponse(s int, w http.ResponseWriter, body interface{}) {
	w.WriteHeader(s)
	w.Header().Set("Content-type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.SetEscapeHTML(false)
	_ = encoder.Encode(body)
}

func writeErrorResponse(s int, w http.ResponseWriter, err error) {
	writeJSONResponse(s, w, msg(err.Error()))
}

func appidFromReq(r *http.Request) string {
	v := mux.Vars(r)
	return v["appid"]
}
