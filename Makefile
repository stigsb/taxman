all: test taxman

.PHONY: generate coverage taxman fmt vet test

TEST_PKGS=./pkg/apis/... ./cmd/...

test: generate fmt vet
	go test -v -coverprofile cover.out $(TEST_PKGS) | tee test.out
	cat cover.out | ./hack/coverage-without-generated-code.sh > cover-nongen.out
	go tool cover -func cover-nongen.out | grep '^total:'

coverage: test
	go tool cover -html=cover-nongen.out

taxman: generate fmt vet
	go build -o bin/taxman ./cmd/taxman

fmt:
	go fmt ./pkg/... ./cmd/...

vet:
	go vet ./pkg/... ./cmd/...

generate:
	sed -e 's,^,// ,; s,  *$$,,' LICENSE.txt > hack/boilerplate.go.txt
	./hack/update-client.sh
