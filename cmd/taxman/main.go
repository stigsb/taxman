// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package main

import (
	"context"
	"flag"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/blendle/zapdriver"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/weaveworks/common/middleware"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"sigs.k8s.io/controller-runtime/pkg/client/config"

	"gitlab.com/zedge-oss/zeppa/taxman/pkg/apis/taxman"
	apiv2 "gitlab.com/zedge-oss/zeppa/taxman/pkg/apis/taxman/v2"

	dwhv1betaclientset "gitlab.com/zedge-oss/zeppa/taxman/pkg/client/clientset/typed/dwh/v1beta1"
)

var requestDuration = prometheus.NewHistogramVec(prometheus.HistogramOpts{
	Name:    "http_request_seconds",
	Help:    "Time (in seconds) spent serving HTTP requests",
	Buckets: prometheus.DefBuckets,
}, []string{"method", "path", "status", "ws"})

func init() {
	prometheus.MustRegister(
		requestDuration,
	)
}

func main() {
	var myNamespace, listenAddress string
	var wait time.Duration
	flag.StringVar(&myNamespace, "namespace", "", "Kubernetes namespace to get event taxonomies from")
	flag.StringVar(&listenAddress, "listen", ":8080", "listen address (:port or ip:port)")
	flag.DurationVar(&wait, "graceful-timeout", time.Second*15, "how long to wait for pending connections to finish while shutting down gracefully")
	flag.Parse()

	tmp, err := zapdriver.NewProduction()
	if err != nil {
		panic(err)
	}
	log := tmp.Sugar()
	cfg := config.GetConfigOrDie()
	client := dwhv1betaclientset.NewForConfigOrDie(cfg).EventTaxonomies(myNamespace)
	v1 := taxman.Handlers{
		Namespace: myNamespace,
		Client:    client,
		Log:       log,
	}
	v2 := apiv2.Handlers{
		Namespace: myNamespace,
		Client:    client,
		Log:       log,
	}
	r := mux.NewRouter()
	v1.Register(r)
	v2.Register(r)
	srv := &http.Server{
		Addr:         listenAddress,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      middleware.Instrument{Duration: requestDuration}.Wrap(r),
	}

	go func() {
		log.Infow("listening for connections", "address", listenAddress)
		if err := srv.ListenAndServe(); err != nil {
			log.Panic(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	<-c

	log.Info("waiting for pending connections")
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()

	_ = srv.Shutdown(ctx)
	log.Info("shutting down")
	os.Exit(0)
}
