module gitlab.com/zedge-oss/zeppa/taxman

go 1.13

require (
	github.com/blendle/zapdriver v1.1.6
	github.com/fgrosse/zaptest v1.1.0
	github.com/go-logr/logr v0.1.0
	github.com/gogo/googleapis v1.3.0 // indirect
	github.com/gogo/status v1.1.0 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/jstemmer/go-junit-report v0.9.1
	github.com/kshvakov/clickhouse v1.3.5 // indirect
	github.com/onsi/ginkgo v1.10.3
	github.com/onsi/gomega v1.5.0
	github.com/opentracing-contrib/go-stdlib v0.0.0-20190519235532-cf7a6c988dc9 // indirect
	github.com/opentracing/opentracing-go v1.1.0 // indirect
	github.com/ory/dockertest v3.3.2+incompatible
	github.com/prometheus/client_golang v0.9.2
	github.com/stretchr/testify v1.3.0
	github.com/uber/jaeger-client-go v2.20.1+incompatible // indirect
	github.com/uber/jaeger-lib v2.2.0+incompatible // indirect
	github.com/weaveworks/common v0.0.0-20190110153500-81a1a4d158e6
	github.com/weaveworks/promrus v1.2.0 // indirect
	gitlab.com/zedge-oss/zeppa/event-taxonomy v1.1.40
	go.uber.org/zap v1.9.1
	google.golang.org/grpc v1.25.1 // indirect
	k8s.io/api v0.0.0-20190409021203-6e4e0e4f393b
	k8s.io/apimachinery v0.0.0-20190404173353-6a84e37a896d
	k8s.io/client-go v11.0.1-0.20190409021438-1a26190bd76a+incompatible
	k8s.io/code-generator v0.0.0-20190704094409-6c2a4329ac29
	k8s.io/gengo v0.0.0-20191108084044-e500ee069b5c // indirect
	sigs.k8s.io/controller-runtime v0.2.2
)
