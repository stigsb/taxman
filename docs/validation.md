# Taxonomy Change Validation

## Properties

These are the rules for properties:

 1. Properties with `scope: Internal` or `scope: Envelope` may not be modified
 2. Type change rules depend on the `lifecycle` of the property:
    * `lifecycle: PreProduction`: you can change types freely (but keep in mind that this can give you
       errors in Clickhouse if you start getting both `Int32` and `Array(String)` in the same column).
    * `lifecycle: Production`: and the type change is "safe" (i.e. non-destructive). Examples of safe type
      changes are Int8 -> Int16, Int32 -> String. Basically any type change you can do without information
      loss is "safe".
    * `lifecycle: ProductionEndOfLife`: no type changes are allowed.
 3. Lifecycle changes are restricted to the following:
    * `PreProduction` -> `Production`
    * `Production` -> `ProductionEndOfLife`
    * `ProductionEndOfLife` -> `Production`
 4. You may only remove properties if:
    * their lifecycle is `PreProduction`
    * their lifecycle is `ProductionEndOfLife` and enough time has passsed since they were last modified (25 months)

## Enums

These are the rules for enum fields:

 1. Enum fields may not change their numerical value.
 2. Enum lifecycle changes must follow the same rules as for properties.
 3. Enum fields with `lifecycle: Production` may not be removed (they need to go through the `ProductionEndOfLife`
    transition period).

## Twin Taxonomies

Twin taxonomies are typically used for keeping the same app on different platforms in sync, allowing them to make
independent changes, but stopping them from making conflicting ones.

The additional change validation rules for twin taxonomies are:

 1. Every property that exist in both taxonomies must have the same `scope` and `type`. "Safe" type differences
    are not allowed here, because even if Int8 -> Int16 is safe, Int16 -> Int8 is not.
 2. Enum fields that exist in both must have the same numerical value (for example, you can not add `Click=1` in
    yours if your twin already has `Click=2`).
 3. Enum field values may not be re-used by another enum field (for example, you can not add `Download=2` in yours
    if your twin already has `Click=2`).
