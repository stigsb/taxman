# Event Taxonomies

The example above is a payload format, and doesn't tell you which fields have which types, scopes or lifecycles. This is where
the Event Taxonomy comes in.

The taxonomy describes, per `appid`, which properties exist, and which enums are supported. (It also defines internal data bindings for the
event pipelines such as Kafka topics and ClickHouse tables, but these details are not exposed to clients.)

## Event Properties

Event properties are defined like this:

```json
{
  "name": "section",
  "scope": "Event",
  "lifecycle": "Production",
  "last_modified": "2018-12-13T12:59:27Z",
  "type": "String"
}
```

Allowed types are: `String`, `Bool`, `Int8`, `Int16`, `Int32`, `Int64`, `Enum:Foo`, `Date`, `DateTime`, `Float32` and
`Float64`. In addition, `Nullable(Type)` is allowed for all these, as well as `Array(Type)` and `Array(Nullable(Type))`.
The `Enum:Foo` example refers to an enum called `Foo`, which must be defined in the same taxonomy. All integer types
are signed.

## Event Enums

Enums provide a type-safe and storage-efficient way of representing values from an enumerated set. Enums have a `"name"` and a list of `"fields"`:

```json
{
  "name": "ContentType",
  "fields": [
    {
      "name": "WALLPAPER",
      "value": 1,
      "lifecycle": "Production",
      "last_modified": "2018-12-14T15:02:39Z"
    },
    {
      "name": "RINGTONE",
      "value": 4,
      "lifecycle": "Production",
      "last_modified": "2018-12-14T15:02:39Z"
    }
  ]
}
```

## Example Event Taxonomy

This is an (abridged) example event taxonomy:

```json
{
  "appid": "android",
  "enums": [
    {
      "name": "Event",
      "fields": [
        {
          "name": "CLICK",
          "value": 1,
          "lifecycle": "ProductionEndOfLife",
          "last_modified": "2018-12-13T12:59:27Z"
        },
        {
          "name": "BROWSE",
          "value": 2,
          "lifecycle": "ProductionEndOfLife",
          "last_modified": "2018-12-14T15:02:39Z"
        },
        {
          "name": "marketplace_open",
          "value": 5,
          "lifecycle": "Production",
          "last_modified": "2018-12-13T12:59:27Z"
        },
        {
          "name": "marketplace_artist_impression",
          "value": 6,
          "lifecycle": "Production",
          "last_modified": "2018-12-13T12:59:27Z"
        }
      ]
    },
    {
      "name": "ContentType",
      "fields": [
        {
          "name": "WALLPAPER",
          "value": 1,
          "lifecycle": "Production",
          "last_modified": "2018-12-14T15:02:39Z"
        },
        {
          "name": "RINGTONE",
          "value": 4,
          "lifecycle": "Production",
          "last_modified": "2018-12-14T15:02:39Z"
        }
      ]
    }
  ],
  "properties": [
    {
      "name": "server_timestamp",
      "scope": "Envelope",
      "lifecycle": "Production",
      "last_modified": "2018-12-01T00:00:00Z",
      "type": "Int64"
    },
    {
      "name": "client_time_ms",
      "scope": "Internal",
      "lifecycle": "Production",
      "last_modified": "2018-12-01T00:00:00Z",
      "type": "Int64"
    },
    {
      "name": "zid",
      "scope": "Envelope",
      "lifecycle": "Production",
      "last_modified": "2018-01-02T00:00:00Z",
      "type": "String"
    },
    {
      "name": "country_iso",
      "scope": "Internal",
      "lifecycle": "Production",
      "last_modified": "2018-12-01T00:00:00Z",
      "type": "String"
    },
    {
      "name": "experiment",
      "scope": "Internal",
      "lifecycle": "Production",
      "last_modified": "2018-12-01T00:00:00Z",
      "type": "String"
    },
    {
      "name": "import_filename",
      "scope": "Internal",
      "lifecycle": "Production",
      "last_modified": "2018-12-01T00:00:00Z",
      "type": "String"
    },
    {
      "name": "section",
      "scope": "Event",
      "lifecycle": "Production",
      "last_modified": "2018-12-13T12:59:27Z",
      "type": "String"
    },
    {
      "name": "offset",
      "scope": "Event",
      "lifecycle": "Production",
      "last_modified": "2018-12-13T12:59:27Z",
      "type": "Int16"
    },
    {
      "name": "from_content_type",
      "scope": "Event",
      "lifecycle": "Production",
      "last_modified": "2018-12-13T12:59:27Z",
      "type": "Enum:ContentType"
    }
  ]
}
```

## Updating Taxonomies

Clients should update their taxonomies from CI builds in their default branch (master or develop), by POST-ing a
request to `https://taxman.zedge.io/` (see [taxman API docs](taxman.md)).

## Twin Taxonomies

A taxonomy may declare another taxonomy as its twin, which adds additional validation rules.
