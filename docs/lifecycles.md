# Lifecycles

Lifecycles are used in Properties and Enum Fields to describe the support level of the property/enum field.

There are three lifecycles:

## PreProduction

This is typically the default lifecycle of new properties. While a property or enum value is in this phase,
it may be changed or removed freely. Type changes are allowed, even ones that would potentially cause problems.

In Clickhouse, columns with `lifecycle: PreProduction` don't appear in the `appid.events` tables, but in
`appid.preprod_events`. You should not use the "preprod" tables for production metrics or reports, as the underlying
schema may change and break your query.

## Production

Once a property reached `lifecycle: Production`, some commitments kick in:

 1. we guarantee 25 months of data retention SLA
 2. we only allow "safe" type changes to the property/column

## ProductionEndOfLife

Since we guarantee 25 months availability SLA for Production properties, when we want to remove a property,
it needs to go through a deprecation period with `lifecycle: ProductionEndOfLife`. When properties are in
this state, no changes are allowed. They can still not be removed until their `last_modified` attribute
is 25 months in the past.
